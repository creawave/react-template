import React from 'react';
import './App.css';
import { useDispatch } from "react-redux";
import { sagaActions } from "./store/actions";
import routes from "./common/routes";
import { Route, Switch } from "react-router-dom";
import Layout from "./common/components/global/Layout";

const App = () => {
    const dispatch = useDispatch();
    return (
        <Layout>
            <Switch>
                <Route exact path={routes.home}>
                    Home
                    <button onClick={() => dispatch({ type: sagaActions.EXAMPLE_TYPE})}> CLICK </button>
                </Route>
                <Route exact path={routes.signUp}>
                    Sign Up
                </Route>
                <Route exact path={routes.logIn}>
                    Log In
                </Route>
                <Route exact path={routes.vipManager}>
                    Vip Manager
                </Route>
                <Route exact path={routes.apply}>
                    T&C Apply
                </Route>
                <Route exact path={routes.loyaltyProgram}>
                    Loyalty Program
                </Route>
            </Switch>
        </Layout>
    )
};

export default App;
