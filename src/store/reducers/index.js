import { createSlice } from "@reduxjs/toolkit";

export const nameSlice = createSlice({
    name: "name",
    initialState: {
        example: [],
        loading: false,
        error: false,
    },
    reducers: {
        exampleRequest: state => {
            state.loading = true;
        },
        exampleSuccess: (state, action) => {
            state.loading = false;
            state.example = action.payload;
        },
        exampleError: state => {
            state.error = false;
        }
    }
});


export const { exampleRequest, exampleSuccess, exampleError } = nameSlice.actions;

export  default  nameSlice;