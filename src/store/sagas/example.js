import { put, call } from 'redux-saga/effects';
import { exampleRequest, exampleSuccess, exampleError } from "../reducers";


export function* fetchDataSaga() {
    try {
        yield put(exampleRequest());
        const data = yield call(() => {
                return fetch('https://dog.ceo/api/breeds/image/random')
                    .then(res => res.json())
            }
        );
        yield put(exampleSuccess(data));
    } catch (e) {
        yield put(exampleError())
    }
}