import { all } from "@redux-saga/core/effects";
import { takeLatest } from "redux-saga/effects";
import { sagaActions } from "../actions";
import { fetchDataSaga } from "./example";

export function* watchAll() {
    yield all([
        yield takeLatest(sagaActions.EXAMPLE_TYPE, fetchDataSaga),
    ]);
}