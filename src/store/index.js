import {
    configureStore,
    getDefaultMiddleware,
} from "@reduxjs/toolkit";
import createSagaMiddleware from "redux-saga";
import { watchAll } from "./sagas/watchAll";
import nameSlice from "./reducers";


let sagaMiddleware = createSagaMiddleware();

const middleware = [...getDefaultMiddleware({thunk: false,}), sagaMiddleware];

const store = configureStore({
    reducer: {
        reducerName: nameSlice.reducer,
    },
    middleware,
});

sagaMiddleware.run(watchAll);

export default store;