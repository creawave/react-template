const routes = {
    home: '/',
    signUp: '/sign-up',
    logIn: '/logIn',
    vipManager: '/vipManager',
    apply: '/apply',
    loyaltyProgram: '/loyaltyProgram',
};

export default routes;