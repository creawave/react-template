import React from 'react';
import { NavLink } from "react-router-dom";
import routes from "../../../routes";

const SideBar = () => {
    return (
        <div>
            <NavLink to={routes.signUp}>Sing UP</NavLink>
            <NavLink to={routes.logIn}>Log In</NavLink>
            <NavLink to={routes.home}>Home</NavLink>
            <NavLink to={routes.vipManager}>Vip Manager</NavLink>
            <NavLink to={routes.apply}>T&C Apply</NavLink>
            <NavLink to={routes.loyaltyProgram}>loyalty Program</NavLink>
        </div>
    )
};

export default SideBar;